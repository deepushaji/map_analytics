
const tileReduce = require('@mapbox/tile-reduce')
var path = require('path');

var numFeatures = 0;

tileReduce({
  bbox: [-122.05862045288086, 36.93768132842635, -121.97296142578124, 37.00378647456494],
  zoom: 15,
  map: path.join(__dirname, '/count.js'),
  sources: [
{
    name: 'streets',
    url: 'https://b.tiles.mapbox.com/v4/mapbox.mapbox-streets-v6/{z}/{x}/{y}.vector.pbf',//'https://tiles.osm-analytics.heigit.org/highways/{z}/{x}/{y}.pbf',
    layers: ['roads'],
    raw: true
  }
]
})
.on('reduce', function(num) {
  numFeatures += num;
})
.on('end', function() {
  console.log('Features total: %d', numFeatures);
});

